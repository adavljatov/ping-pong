package com.adavliatov.web;

import java.io.IOException;
import java.net.ServerSocket;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import com.adavliatov.Runner;
import io.netty.handler.codec.http.HttpResponseStatus;
import io.vertx.core.AbstractVerticle;
import io.vertx.core.DeploymentOptions;
import io.vertx.core.Vertx;
import io.vertx.core.json.Json;
import io.vertx.core.json.JsonObject;
import io.vertx.core.logging.Logger;
import io.vertx.core.logging.LoggerFactory;
import io.vertx.ext.unit.Async;
import io.vertx.ext.unit.TestContext;
import io.vertx.ext.unit.junit.VertxUnitRunner;

/**
 * @author adavliatov
 * @since 28.03.2016
 */
@RunWith(VertxUnitRunner.class)
public class WebAppVerticleTest extends AbstractVerticle {
    private static final Logger log = LoggerFactory.getLogger(WebAppVerticleTest.class);

    private int port = 8088;

    @Before
    public void setUp(TestContext context) {
        vertx = Vertx.vertx();

        ServerSocket socket;
        try {
            socket = new ServerSocket(0);
            port = socket.getLocalPort();
            socket.close();
        } catch (IOException e) {
            log.error(e);
        }

        DeploymentOptions options = new DeploymentOptions().setConfig(new JsonObject().put("http.port", port));
        vertx.deployVerticle(Runner.class.getName(), options, context.asyncAssertSuccess());
    }

    @After
    public void tearDown(TestContext context) {
        vertx.close(context.asyncAssertSuccess());
    }

    @Test
    public void testStaticResources(TestContext context) {
        final Async async = context.async();

        vertx.createHttpClient().getNow(port, "localhost", "/",
                response -> {
                    context.assertEquals(response.statusCode(), HttpResponseStatus.OK.code());
                    context.assertEquals(response.headers().get("content-type"), "text/html");
                    response.bodyHandler(body -> {
                        context.assertTrue(StringUtils.containsIgnoreCase(body.toString(), "<title>Ping-pong app</title>"));
                        async.complete();
                    });
                });
    }

    @Test
    public void testInvalidLocation(TestContext context) {
        final Async async = context.async();
        vertx.createHttpClient().getNow(port, "localhost", "/invalid",
                response -> {
                    context.assertEquals(response.statusCode(), HttpResponseStatus.NOT_FOUND.code());
                    response.bodyHandler(body -> {
                        context.assertTrue(StringUtils.containsIgnoreCase(body.toString(), "Resource not found"));
                        async.complete();
                    });
                });
    }

    @Test
    public void testNoCommandParam(TestContext context) {
        final Async async = context.async();
        final String request = Json.encodePrettily(new JsonObject());
        final String length = Integer.toString(request.length());
        vertx.createHttpClient().post(port, "localhost", "/handler")
                .putHeader("content-type", "application/json")
                .putHeader("content-length", length)
                .handler(response -> {
                    context.assertEquals(response.statusCode(), HttpResponseStatus.BAD_REQUEST.code());
                    response.bodyHandler(body -> {
                        context.assertTrue(StringUtils.containsIgnoreCase(body.toString(), "Empty action"));
                        async.complete();
                    });
                })
                .write(request)
                .end();
    }


    @Test
    public void testInvalidCommandParam(TestContext context) {
        final Async async = context.async();
        final String request = Json.encodePrettily(new JsonObject(Collections.singletonMap("action", "invalidAction")));
        final String length = Integer.toString(request.length());
        vertx.createHttpClient().post(port, "localhost", "/handler")
                .putHeader("content-type", "application/json")
                .putHeader("content-length", length)
                .handler(response -> {
                    context.assertEquals(response.statusCode(), HttpResponseStatus.BAD_REQUEST.code());
                    response.handler(body -> {
                        context.assertTrue(StringUtils.containsIgnoreCase(body.toString(), "Invalid action"));
                        async.complete();
                    });
                })
                .write(request)
                .end();
    }

    @Test
    public void testNoUserId(TestContext context) {
        final Async async = context.async();
        final String request = Json.encodePrettily(new JsonObject(Collections.singletonMap("action", "pong")));
        final String length = Integer.toString(request.length());
        vertx.createHttpClient().post(port, "localhost", "/handler")
                .putHeader("content-type", "application/json")
                .putHeader("content-length", length)
                .handler(response -> {
                    context.assertEquals(response.statusCode(), HttpResponseStatus.BAD_REQUEST.code());
                    response.handler(body -> {
                        context.assertTrue(StringUtils.containsIgnoreCase(body.toString(), "Invalid userId"));
                        async.complete();
                    });
                })
                .write(request)
                .end();
        ;
    }

    @Test
    public void testInvalidUserId(TestContext context) {
        final Async async = context.async();
        Map<String, Object> params = new HashMap<>();
        params.put("action", "pong");
        params.put("userId", null);
        String request = Json.encodePrettily(new JsonObject(params));
        String length = Integer.toString(request.length());
        vertx.createHttpClient().post(port, "localhost", "/handler")
                .putHeader("content-type", "application/json")
                .putHeader("content-length", length).handler(
                response -> {
                    context.assertEquals(response.statusCode(), HttpResponseStatus.BAD_REQUEST.code());
                    response.handler(body -> {
                        context.assertTrue(StringUtils.containsIgnoreCase(body.toString(), "Invalid userId"));
                    });
                })
                .write(request)
                .end();
        ;
        //let's assume userId is numeric
        params.put("userId", "let's assume userId is numeric");
        request = Json.encodePrettily(new JsonObject(params));
        length = Integer.toString(request.length());
        vertx.createHttpClient().post(port, "localhost", "/handler")
                .putHeader("content-type", "application/json")
                .putHeader("content-length", length).handler(
                response -> {
                    context.assertEquals(response.statusCode(), HttpResponseStatus.BAD_REQUEST.code());
                    response.handler(body -> {
                        context.assertTrue(StringUtils.containsIgnoreCase(body.toString(), "Invalid userId"));
                        async.complete();
                    });
                })
                .write(request)
                .end();
    }
}
