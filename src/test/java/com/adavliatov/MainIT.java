package com.adavliatov;

import com.jayway.restassured.RestAssured;
import com.jayway.restassured.http.ContentType;
import io.netty.handler.codec.http.HttpResponseStatus;
import io.vertx.core.json.Json;
import io.vertx.core.json.JsonObject;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

import java.util.HashMap;
import java.util.Map;

import static com.jayway.restassured.RestAssured.given;
import static org.assertj.core.api.Assertions.assertThat;

/**
 * @author adavliatov
 * @since 01.04.2016
 */
public class MainIT {
    @BeforeClass
    public static void configureRestAssured() {
        RestAssured.baseURI = "http://localhost";
        RestAssured.port = Integer.getInteger("http.port", 8088);
    }

    @AfterClass
    public static void unconfigureRestAssured() {
        RestAssured.reset();
    }

    @Test
    public void checkValidRequest() {
        Map<String, Object> params = new HashMap<>();
        params.put("action", "pong");
        params.put("userId", "123");
        JsonObject requestParams = new JsonObject(params);
        ResponseWrapper response1 = given().contentType(ContentType.JSON).body(Json.encodePrettily(requestParams)).expect().statusCode(HttpResponseStatus.OK.code()).contentType(ContentType.JSON).when().post("/handler").thenReturn().as(ResponseWrapper.class);
        assertThat(response1.getCount() != null);
        assertThat(response1.getCount() > 0);
        ResponseWrapper response2 = given().contentType(ContentType.JSON).body(Json.encodePrettily(requestParams)).expect().statusCode(HttpResponseStatus.OK.code()).contentType(ContentType.JSON).when().post("/handler").thenReturn().as(ResponseWrapper.class);
        assertThat(response2.getCount() != null);
        assertThat(response2.getCount() > 0);
        assertThat(response2.getCount() > response1.getCount());
    }

    private static class ResponseWrapper {
        private Integer count;

        public Integer getCount() {
            return count;
        }

        public void setCount(Integer count) {
            this.count = count;
        }
    }
}
