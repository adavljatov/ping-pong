package com.adavliatov.web;

import io.netty.handler.codec.http.HttpResponseStatus;
import io.vertx.core.AbstractVerticle;
import io.vertx.core.AsyncResult;
import io.vertx.core.Future;
import io.vertx.core.eventbus.DeliveryOptions;
import io.vertx.core.eventbus.Message;
import io.vertx.core.json.JsonObject;
import io.vertx.core.logging.Logger;
import io.vertx.core.logging.LoggerFactory;
import io.vertx.ext.web.Router;
import io.vertx.ext.web.RoutingContext;
import io.vertx.ext.web.handler.BodyHandler;
import io.vertx.ext.web.handler.StaticHandler;

/**
 * @author adavliatov
 * @since 28.03.2016
 */
public class WebAppVerticle extends AbstractVerticle {
    private static final Logger log = LoggerFactory.getLogger(WebAppVerticle.class);

    @Override
    public void start(Future<Void> fut) {
        int port = config().getInteger("http.port", 8088);
        log.info("Server is started. Go to http://localhost:" + port + " location");
        final DeliveryOptions opts = new DeliveryOptions();
        if (config().containsKey("timeout")) {
            opts.setSendTimeout(config().getLong("timeout", 5000L));
        }

        Router router = Router.router(vertx);
        router.route("/").handler(routingContext -> {
            routingContext.reroute("/assets");
        });
        router.route("/assets/*").handler(StaticHandler.create("assets"));
        router.route().handler(BodyHandler.create());
        router.route("/handler").
                handler(routingContext -> {
                    vertx.eventBus().<JsonObject>send("serviceWorker", routingContext.getBodyAsJson(), opts, reply -> handleReply(reply, routingContext));
                });
        vertx.createHttpServer().
                requestHandler(router::accept).listen(
                port,
                result -> {
                    if (result.succeeded()) fut.complete();
                    else fut.fail(result.cause());
                });
    }

    private void handleReply(AsyncResult<Message<JsonObject>> reply, RoutingContext rc) {
        if (reply.succeeded()) {
            Message<JsonObject> replyMsg = reply.result();
            rc.response()
                    .setStatusMessage("OK")
                    .setStatusCode(HttpResponseStatus.OK.code())
                    .putHeader("Content-Type", "application/json")
                    .end(replyMsg.body().toString());
        } else {
            rc.response()
                    .setStatusCode(HttpResponseStatus.BAD_REQUEST.code())
                    .setStatusMessage("Invalid request")
                    .end(reply.cause().getLocalizedMessage());
        }
    }
}
