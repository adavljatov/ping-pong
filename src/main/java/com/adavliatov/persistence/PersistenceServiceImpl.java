package com.adavliatov.persistence;

import io.vertx.core.AsyncResult;
import io.vertx.core.Handler;
import io.vertx.core.Vertx;
import io.vertx.core.json.JsonObject;
import io.vertx.redis.RedisClient;
import io.vertx.redis.RedisOptions;

/**
 * @author adavliatov
 * @since 05.04.2016
 */
public class PersistenceServiceImpl implements PersistenceService {
    private final RedisClient redis;

    public PersistenceServiceImpl(Vertx vertx, JsonObject config) {
        String redisHost = config.getString("redis.host", "localhost");
        int redisPort = config.getInteger("redis.port", 6379);
        RedisOptions redisOptions = new RedisOptions().setHost(redisHost).setPort(redisPort);
        this.redis = RedisClient.create(vertx, redisOptions);
    }

    @Override
    public void incr(String key, Handler<AsyncResult<Long>> handler) {
        redis.incr(key, handler);
    }
}