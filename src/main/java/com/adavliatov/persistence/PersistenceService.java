package com.adavliatov.persistence;

import io.vertx.codegen.annotations.ProxyGen;
import io.vertx.codegen.annotations.VertxGen;
import io.vertx.core.AsyncResult;
import io.vertx.core.Handler;
import io.vertx.core.Vertx;
import io.vertx.core.json.JsonObject;
import io.vertx.serviceproxy.ProxyHelper;

/**
 * @author adavliatov
 * @since 04.04.2016
 */
@VertxGen
@ProxyGen
public interface PersistenceService {
    static PersistenceService create(Vertx vertx, JsonObject config) {
        return new PersistenceServiceImpl(vertx, config);
    }

    static PersistenceService createProxy(Vertx vertx, String address) {
        return ProxyHelper.createProxy(PersistenceService.class, vertx, address);
    }

    void incr(String key, Handler<AsyncResult<Long>> result);
}
