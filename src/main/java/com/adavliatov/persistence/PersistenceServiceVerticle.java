package com.adavliatov.persistence;

import io.vertx.core.AbstractVerticle;
import io.vertx.core.DeploymentOptions;
import io.vertx.core.Vertx;
import io.vertx.core.json.JsonObject;
import io.vertx.core.logging.Logger;
import io.vertx.core.logging.LoggerFactory;
import io.vertx.serviceproxy.ProxyHelper;

/**
 * @author adavliatov
 * @since 04.04.2016
 */
public class PersistenceServiceVerticle extends AbstractVerticle {
    private static final Logger log = LoggerFactory.getLogger(PersistenceServiceVerticle.class);
    PersistenceService service;

    public static void main(String[] args) {
        Vertx vertx = Vertx.vertx();
        vertx.deployVerticle(
                PersistenceServiceVerticle.class.getName(),
                new DeploymentOptions().setConfig(new JsonObject()
                        .put("redis.host", "localhost")));
    }

    @Override
    public void start() throws Exception {
        service = PersistenceService.create(vertx, config());
        ProxyHelper.registerService(
                PersistenceService.class, vertx, service,
            "servicePersistence");
    }
}