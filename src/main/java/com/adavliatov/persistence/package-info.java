/**
 * @author adavliatov.
 * @since 15.07.2017.
 */
@ModuleGen(name = "servicePersistence", groupPackage = "com.adavliatov.persistence")
package com.adavliatov.persistence;

import io.vertx.codegen.annotations.ModuleGen;