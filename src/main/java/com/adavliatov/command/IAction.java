package com.adavliatov.command;

import io.netty.handler.codec.http.HttpResponseStatus;
import io.vertx.core.Handler;
import io.vertx.core.Vertx;
import io.vertx.core.eventbus.Message;
import io.vertx.core.json.JsonObject;

/**
 * @author adavliatov
 * @since 05.04.2016
 */
//Actually we could use reflection or another pattern implementation.. But with Java 8 it is already very conscice and simple.

public interface IAction extends Handler<Message<JsonObject>> {
    IAction NOT_FOUND_ACTION = message -> {
        message.fail(HttpResponseStatus.NOT_IMPLEMENTED.code(), "Invalid action");
    };

    static IAction parse(Vertx vertx, String action) {
        switch (action) {
            case "pong":
                return new PongAction(vertx);
            default:
                return NOT_FOUND_ACTION;
        }
    }
}
