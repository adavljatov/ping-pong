package com.adavliatov.command;

/**
 * @author adavliatov
 * @since 30.03.2016
 */
public abstract class Action implements IAction {
    private final Long code;
    private final String name;

    protected Action(Long code, String name) {
        this.code = code;
        this.name = name;
    }

    public Long getCode() {
        return code;
    }

    public String getName() {
        return name;
    }
}
