package com.adavliatov.command;

import io.netty.handler.codec.http.HttpResponseStatus;
import io.vertx.core.AbstractVerticle;
import io.vertx.core.json.JsonObject;
import io.vertx.core.logging.Logger;
import io.vertx.core.logging.LoggerFactory;
import org.apache.commons.lang3.StringUtils;

/**
 * @author adavliatov
 * @since 30.03.2016
 */
public class ActionWorkerVerticle extends AbstractVerticle {
    private static final Logger log = LoggerFactory.getLogger(ActionWorkerVerticle.class);

    @Override
    public void start() throws Exception {
        vertx.eventBus().<JsonObject>consumer("serviceWorker").handler(message -> {
            if (!message.body().containsKey("action")) {
                message.fail(HttpResponseStatus.BAD_REQUEST.code(), "Empty action");
                        return;
                    }
            Object command = message.body().getValue("action");
                    if (!(command instanceof CharSequence) || StringUtils.isBlank((String) command)) {
                        message.fail(HttpResponseStatus.NOT_IMPLEMENTED.code(), "Action not implemented yet: " + command);
                        log.error("Action not implemented yet: " + command);
                        return;
                    }
            IAction action = IAction.parse(vertx, (String) command);
            action.handle(message);
                }
        );
    }
}
