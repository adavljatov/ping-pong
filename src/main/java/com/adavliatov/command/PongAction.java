package com.adavliatov.command;

import com.adavliatov.persistence.PersistenceService;
import com.adavliatov.web.WebAppVerticle;
import io.netty.handler.codec.http.HttpResponseStatus;
import io.vertx.core.Vertx;
import io.vertx.core.eventbus.Message;
import io.vertx.core.json.JsonObject;
import io.vertx.core.logging.Logger;
import io.vertx.core.logging.LoggerFactory;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.math.NumberUtils;

import java.util.Collections;

/**
 * @author adavliatov
 * @since 30.03.2016
 */
public class PongAction extends Action {
    private static final Logger log = LoggerFactory.getLogger(WebAppVerticle.class);

    private static final Long CODE = 1L;
    private static final String NAME = "pong";

    private PersistenceService persistenceService;

    public PongAction(Vertx vertx) {
        super(CODE, NAME);
        this.persistenceService = PersistenceService.createProxy(vertx, "servicePersistence");
    }

    @Override
    public void handle(final Message<JsonObject> message) {
        Object userIdParam = message.body().getValue("userId");
        if (!(userIdParam instanceof CharSequence) || !StringUtils.isNumeric((CharSequence) userIdParam)) {
            message.fail(HttpResponseStatus.BAD_REQUEST.code(), "Invalid userId: " + userIdParam);
            log.error("Invalid userId");
            return;
        }
        Long userId = NumberUtils.createLong(userIdParam.toString());
        persistenceService.incr(String.valueOf("pong:" + userId), r -> {
            if (r.succeeded()) {
                JsonObject response = new JsonObject(Collections.singletonMap("count", r.result()));
                message.reply(response);
            } else {
                log.error("Connection or Operation Failed " + r.cause());
                message.fail(HttpResponseStatus.INTERNAL_SERVER_ERROR.code(), "Connection or Operation Failed " + r.cause());
            }
        });
    }
}
