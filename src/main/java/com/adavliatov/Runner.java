package com.adavliatov;

import com.adavliatov.command.ActionWorkerVerticle;
import com.adavliatov.persistence.PersistenceServiceVerticle;
import com.adavliatov.web.WebAppVerticle;
import io.vertx.core.AbstractVerticle;
import io.vertx.core.AsyncResult;
import io.vertx.core.AsyncResultHandler;
import io.vertx.core.DeploymentOptions;
import io.vertx.core.Future;
import io.vertx.core.Vertx;
import io.vertx.core.json.JsonObject;
import io.vertx.core.logging.Logger;
import io.vertx.core.logging.LoggerFactory;

/**
 * @author adavliatov
 * @since 03.04.2016
 */
public class Runner extends AbstractVerticle {

    private static final Logger log = LoggerFactory.getLogger(Runner.class);

    public static void main(String[] args) {
        Vertx vertx = Vertx.vertx();
        JsonObject configs = new JsonObject();
        configs.put("redis.host", "localhost");
        configs.put("http.port", "8088");
        vertx.deployVerticle(Runner.class.getName(), new DeploymentOptions(configs));
    }

    @Override
    public void start(Future<Void> startFuture) throws Exception {
        final DeploymentOptions options = new DeploymentOptions().setConfig(config());
        startPersistence((none0) -> {
            startBackend((none1) -> {
                startWebApp(
                        (none2) -> {
                            completeStartup(none2, startFuture);
                        }, options, startFuture
                );
            }, options.setWorker(true).setInstances(2), startFuture);
        }, options, startFuture);
    }

    private void startPersistence(AsyncResultHandler<Void> next, DeploymentOptions options, Future<Void> future) {
        vertx.deployVerticle(PersistenceServiceVerticle.class.getName(), options, res -> {
            if (res.succeeded()) {
                next.handle(Future.succeededFuture());
            } else {
                log.error("PersistenceVerticle deployment failed!" + res.cause());
                future.fail(res.cause());
            }
        });
    }

    private void startBackend(AsyncResultHandler<Void> next, DeploymentOptions options, Future<Void> future) {
        vertx.deployVerticle(ActionWorkerVerticle.class.getName(), options, res -> {
            if (res.succeeded()) {
                next.handle(Future.succeededFuture());
            } else {
                log.error("ActionWorker deployment failed!" + res.cause());
                future.fail(res.cause());
            }
        });
    }

    private void startWebApp(AsyncResultHandler<Void> next, DeploymentOptions options, Future<Void> future) {
        vertx.deployVerticle(WebAppVerticle.class.getName(), options, res -> {
            if (res.succeeded()) {
                next.handle(Future.succeededFuture());
            } else {
                log.error("WebVerticle deployment failed!" + res.cause());
                future.fail(res.cause());
            }
        });
    }

    private void completeStartup(AsyncResult<Void> res, Future<Void> fut) {
        if (res.succeeded()) {
            log.info("Application started");
            fut.complete();
        } else {
            fut.fail(res.cause());
        }
    }
}
