# Ping-pong miniapp #

Ping-pong test app implementation

### Technologies ###

* [Vertx.io](http://vertx.io) - an async, event-driven, non-blocking framework based on Netty server
* [Redis](http://redis.io/) - an open source (BSD licensed), in-memory data structure store, used as database, cache and message broker.
* [RestAssured](https://github.com/jayway/rest-assured) - testing and validation of REST services in Java
* Build tools: maven
* CVS: git
* Helper libs: apache-commons-lang3, log4j..

### Build & run ###

* _Build:_ 
`mvn clean verify package`
* _Run:_  
`java -jar target/pong-1.0-SNAPSHOT-fat.jar [ -conf src/main/resources/app_conf.json][-Dlog4j.configurationFile=file:src/main/resources/log4j.properties -Dorg.vertx.logger-delegate-factory-class-name=org.vertx.java.core.logging.impl.Log4jLogDelegateFactory ]`
_[] - optional_

### Configuration ###
_Example:_
app_conf.json

{
  "http.port" : 8088,
  "redis.host" : "dgvg.local"
}

### Summary ###
The required implementation is provided.
Unit and integration tests are implemented.

### Author ###
Alexander Davliatov
skype: alex_davljatov
email: alexander.davliatov@gmail.com